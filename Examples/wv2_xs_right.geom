adjustment_0.adj_param_0.center:  0
adjustment_0.adj_param_0.description:  intrack_offset
adjustment_0.adj_param_0.lock_flag:  0
adjustment_0.adj_param_0.parameter:  0
adjustment_0.adj_param_0.sigma:  50
adjustment_0.adj_param_0.units:  pixel
adjustment_0.adj_param_1.center:  0
adjustment_0.adj_param_1.description:  crtrack_offset
adjustment_0.adj_param_1.lock_flag:  0
adjustment_0.adj_param_1.parameter:  0
adjustment_0.adj_param_1.sigma:  50
adjustment_0.adj_param_1.units:  pixel
adjustment_0.adj_param_2.center:  0
adjustment_0.adj_param_2.description:  intrack_scale
adjustment_0.adj_param_2.lock_flag:  0
adjustment_0.adj_param_2.parameter:  0
adjustment_0.adj_param_2.sigma:  50
adjustment_0.adj_param_2.units:  unknown
adjustment_0.adj_param_3.center:  0
adjustment_0.adj_param_3.description:  crtrack_scale
adjustment_0.adj_param_3.lock_flag:  0
adjustment_0.adj_param_3.parameter:  0
adjustment_0.adj_param_3.sigma:  50
adjustment_0.adj_param_3.units:  unknown
adjustment_0.adj_param_4.center:  0
adjustment_0.adj_param_4.description:  map_rotation
adjustment_0.adj_param_4.lock_flag:  0
adjustment_0.adj_param_4.parameter:  0
adjustment_0.adj_param_4.sigma:  0.1
adjustment_0.adj_param_4.units:  degrees
adjustment_0.description:  Initial adjustment
adjustment_0.dirty_flag:  0
adjustment_0.number_of_params:  5
bias_error:  0
ce90_absolute:  0
ce90_relative:  0
current_adjustment:  0
height_off:  379
height_scale:  501
image_id:  10SEP20112558-M1BS_R1C1-052366824010_01_P001
lat_off:  43.3012
lat_scale:  0.0766
line_den_coeff_00:  1
line_den_coeff_01:  0.0008306962
line_den_coeff_02:  0.001891142
line_den_coeff_03:  -0.0001508441
line_den_coeff_04:  5.098113e-05
line_den_coeff_05:  -8.252658e-07
line_den_coeff_06:  -4.715592e-06
line_den_coeff_07:  -1.559408e-05
line_den_coeff_08:  0.0001034489
line_den_coeff_09:  -2.771826e-05
line_den_coeff_10:  -4.861572e-06
line_den_coeff_11:  1.062152e-06
line_den_coeff_12:  0.0001045468
line_den_coeff_13:  1.914426e-08
line_den_coeff_14:  1.846527e-05
line_den_coeff_15:  0.0002045505
line_den_coeff_16:  4.151255e-07
line_den_coeff_17:  -4.333928e-07
line_den_coeff_18:  -1.416638e-05
line_den_coeff_19:  -1.679429e-07
line_num_coeff_00:  0.006929924
line_num_coeff_01:  -0.2099178
line_num_coeff_02:  -1.181362
line_num_coeff_03:  0.02825961
line_num_coeff_04:  -0.0002753766
line_num_coeff_05:  -2.252992e-05
line_num_coeff_06:  3.643525e-05
line_num_coeff_07:  -0.001419311
line_num_coeff_08:  -0.005246128
line_num_coeff_09:  -1.630862e-07
line_num_coeff_10:  -9.819943e-07
line_num_coeff_11:  4.833131e-06
line_num_coeff_12:  2.926995e-05
line_num_coeff_13:  5.754065e-06
line_num_coeff_14:  2.177346e-05
line_num_coeff_15:  8.631664e-05
line_num_coeff_16:  3.243103e-05
line_num_coeff_17:  -6.89464e-07
line_num_coeff_18:  -5.76509e-06
line_num_coeff_19:  -8.978765e-07
line_off:  3025
line_scale:  3114
ll_lat:  43.2521502651732
ll_lon:  0.733076679613606
long_off:  0.8568
long_scale:  0.1238
lr_lat:  43.2247063279551
lr_lon:  0.980583142228531
meters_per_pixel_x:  2.30091047133687
meters_per_pixel_y:  2.31391090898135
number_lines:  6051
number_of_adjustments:  1
number_samples:  8820
polynomial_format:  B
rand_error:  0
rect:  0 0 8819 6050
ref_point_hgt:  379
ref_point_lat:  43.3017090376637
ref_point_line:  3025
ref_point_lon:  0.856256976770849
ref_point_samp:  4409.5
samp_den_coeff_00:  1
samp_den_coeff_01:  2.207654e-05
samp_den_coeff_02:  -0.0006792211
samp_den_coeff_03:  -0.0002863078
samp_den_coeff_04:  -9.290516e-06
samp_den_coeff_05:  7.231886e-07
samp_den_coeff_06:  1.140743e-06
samp_den_coeff_07:  2.100102e-06
samp_den_coeff_08:  -5.197349e-05
samp_den_coeff_09:  1.464909e-06
samp_den_coeff_10:  2.784374e-08
samp_den_coeff_11:  1.920071e-08
samp_den_coeff_12:  -2.527523e-07
samp_den_coeff_13:  0
samp_den_coeff_14:  -1.531271e-08
samp_den_coeff_15:  1.88133e-06
samp_den_coeff_16:  0
samp_den_coeff_17:  -2.320487e-08
samp_den_coeff_18:  -1.046543e-08
samp_den_coeff_19:  0
samp_num_coeff_00:  0.003983012
samp_num_coeff_01:  0.9832835
samp_num_coeff_02:  -0.0005998999
samp_num_coeff_03:  0.01813547
samp_num_coeff_04:  0.0006985558
samp_num_coeff_05:  0.0002162929
samp_num_coeff_06:  -3.217129e-05
samp_num_coeff_07:  -0.004008767
samp_num_coeff_08:  -0.0009206644
samp_num_coeff_09:  6.521324e-06
samp_num_coeff_10:  -8.389453e-07
samp_num_coeff_11:  1.579583e-05
samp_num_coeff_12:  5.372801e-05
samp_num_coeff_13:  1.504456e-06
samp_num_coeff_14:  2.041699e-05
samp_num_coeff_15:  0.0002129405
samp_num_coeff_16:  1.229998e-08
samp_num_coeff_17:  -3.335138e-06
samp_num_coeff_18:  -1.341968e-05
samp_num_coeff_19:  0
samp_off:  4411
samp_scale:  4492
sensor:  WV02
support_data.TDI_level:  14
support_data.absCalFactor:  0.01260825
support_data.azimuth_angle:  171.1
support_data.band_id:  MS1
support_data.band_name_list:  B G R N
support_data.bits_per_pixel:  16
support_data.elevation_angle:  47.6
support_data.generation_date:  2010-09-20T12:58:23.000000Z
support_data.sat_azimuth_angle:  321.3
support_data.sat_elevation_angle:  59.4
support_data.sat_id:  WV02
support_data.tlc_date:  2010-09-20T11:25:58.737050Z;
support_data.type:  ossimQuickbirdMetaData
type:  ossimQuickbirdRpcModel
ul_lat:  43.3778586844478
ul_lon:  0.733514813642385
ur_lat:  43.3509532326615
ur_lon:  0.980323464777898
