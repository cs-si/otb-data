Lithology      Rhyolite
Sample#        AP-939-223
Chemistry
SiO2           78.23
Al2O3          11.73
Fe             0.78
CaO            0.10
TiO2           0.09
MnO            0.01
K2O            7.47
MgO            0.16
Na2O           0.23
P2O5           0.02
PPC            1.27
X-Ray Diffraction
               Quartz
               Potassium Feldspar
               Mica
