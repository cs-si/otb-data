Lithology      Rhyolite
Sample#        AP-981-176
Chemistry
SiO2           81.14
Al2O3          10.89
Fe             0.94
CaO            0.20
TiO2           0.12
MnO            0.01
K2O            0.87
MgO            0.10
Na2O           4.66
P2O5           0.14
PPC            0.96
X-Ray Diffraction
               Quartz
               Plagioclase
               Mica
               Chlorite
