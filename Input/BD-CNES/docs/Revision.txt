6/2/99 Version 1.2

6/1/99
Changed description for Hematite to note spectrum contained water.

5/24/99
Replaced all occurrences where file on additional information line
was *.doc with *.txt. This applied to JHU igneous, metamorphic and
soils.

5/22/99
Added 22 igneous spectra from USGS (Middle East Basalts).
Added 3 metamorphic spectra from USGS (Spain Pyrite Belt).
Added 9 gold spectra from JPL.
Added 19 brass spectra from JPL.
Updated JPL paint spectra:
1247, 1249, 1601, 1602, 1949, 3101c10a, 7211, da67
dm 625, dm725, 1246
Y axis values were incorrectly labelled.

10/29/98 Version 1.1
Added 7 phosphate spectra from USGS.
Added 3 sulfate spectra from USGS.
Added 1 carbonate spectrum from USGS.
Added 11 paint spectra from JPL.
Added 17 soil spectra from R. Hewson, measured by JPL.

08/07/98 
JHU Nicolet Minerals only
1) Replaced all .doc with .txt
2) Change reflectance to transmission in alunit4t.txt, anorth1t.txt and aragon1t.txt.
3) Removed inosilicate acmite1c.txt because incomplete.
4) Removed epidot1t.txt because incomplete.

07/13/98 Version 1.0

07/10/98
USGS update. Perknic-Rocks-Igneous-Cup.

05/11/98
Put all JPL nicolet mineral spectra in percent reflectance.
