Massive, friable, slightly sticky and plastic and strongly effervescent.  1.03% 
organic carbon, 11.1% clay, 50.5% silt, 38.4% sand.  0.5% extractable Fe.  

Clay mineralogy: none.

Coarse mineralogy: none.  15.5% CaCO3, 3.5% CaSO4. 
