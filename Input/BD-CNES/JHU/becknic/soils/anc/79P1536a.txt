Weak medium subangular blocky structure, soft, friable, slightly sticky and 
plastic and strongly effervescent with a clear smooth boundary.  0.5% 
organic carbon, 0.6% extractable Fe.  10.7% clay, 40.4% silt, 48.9% sand.  

Chemical analyses give 21% CaCO3.

Clay mineralogy: none.  

Coarse mineralogy: none.
