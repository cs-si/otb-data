The garnet crystals are rimmed with sillimanite and opaques (magnetite?), and 
occasionally with chlorite instead.  The chlorite, usually associated with 
pyrophyllite, are also very common as alteration products along fractures and 
around sillimanite grains.  Biotite occurs throughout the section, while one 
quartz(?) grain was heavily rutilated.  There was 42% garnet, 16.6% feldspar, 
13.2% quartz, 9.6% sericite & alterations, 7.5% chlorite, 3.7% pyrophyllite, 3.4% 
sillimanite, 3.2% opaques, and 0.8% biotite. 
