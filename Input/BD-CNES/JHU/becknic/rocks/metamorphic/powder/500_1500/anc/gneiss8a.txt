Modes were 61.3% microcline, 11.3% albite, 10.1% orthoclase, 7.4% quartz, 
5.1% alteration (clay and/or sericite), 2.8% biotite, 1.4% amphibole and 0.6% 
opaques. 
