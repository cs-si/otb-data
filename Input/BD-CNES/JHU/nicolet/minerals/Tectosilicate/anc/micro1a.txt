Mineral:  Microcline KAlSi3O8

Sample No.: micro.1

XRD Analysis:  Microcline plus a small amount of albite.

Chemistry:  Microprobe analysis showed the sample to be 
homogeneous within and between grains.  Composition is very close 
to that of pure microcline, as indicated by soda content less than 1%.  
Average of 17 analyses:

SiO2	64.60
Al2O3	18.63
FeO	0.08
MgO	0.04
CaO	0.01
K2O	15.04
Na2O	0.53
TiO2	0.02
MnO	0.03
	_____
Total	98.98
