Mineral:  Sanidine (K,Na)Al3O8

Sample No.:  sanidine.3

XRD Analysis:  Sanidine plus a moderate amount of albite.

Chemistry:  Microprobe analysis showed homogeneity within and 
between grains.  This is typical sanidine with about as much soda as 
potash, but the albite present is cryptoperthitic.  Average of 10 
analyses:

SiO2	67.43
Al2O3	18.65
FeO	0.59
MgO	0.03
CaO	0.02
K2O	7.38
Na2O	5.38
TiO2	0.03
MnO	0.02
	_____
Total	99.97
