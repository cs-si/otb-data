Mineral:  Staurolite (Fe+2,Mg,Zn)2Al9(Si,Al)4O22(OH)2

Sample No.:  staurol.1

XRD Analysis:  Primarily staurolite, plus mangoan almandine, plus 
others.

Chemistry:  Of 10 microprobe analyses, 9 are nominal staurolite and 
1 is ilmenite.  Average of staurolite analyses:

SiO2	27.77
Al2O3	57.61
FeO	13.14
MgO	2.11
CaO	0.01
K2O	0.01
Na2O	0.03
TiO2	0.66
Cr2O3	0.05
	_____
Total	101.39
