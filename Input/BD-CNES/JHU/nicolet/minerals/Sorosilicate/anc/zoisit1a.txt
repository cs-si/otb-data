Mineral:  Zoisite (var. tanzanite) Ca2Al3(SiO4)3(OH)

Sample No.:  zoisite.1

XRD Analysis:  XRD analysis indicated the presence of minor calcite, 
which was dissolved in a sodium acetate bath, after which analysis 
showed pure zoisite.

Chemistry:  Ten microprobe analyses before acetate treatment failed 
to detect calcite and indicated homogeneity within and between 
grains.  The composition agrees with that of zoisite.  Average of 10 
analyses:

SiO2	39.90
Al2O3	35.19
FeO	0.03
MgO	0.06
CaO	25.00
K2O	0.03
Na2O	 0.01
TiO2	0.02
Cr2O3	0.06
	_____
Total	100.30

*  Layer silicate minerals can be conveniently classified on the basis 
of layer type and layer charge. We designate tetrahedral and 
octahedral layering (1:1 and 2:1) and charge per formula unit (x).  
For more information, see Bailey (1980).  
