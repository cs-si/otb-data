Mineral:  Hornblende Ca2(Fe+2,Mg)4Al(Si7Al)O22(OH,F)2

Sample No.: hornblen.1

XRD Analyis:  Hornblende plus trace of unidentifiable mineral.

Chemistry:  Microprobe analysis showed the sample to be 
homogeneous within and between grains and with a composition 
typical of ferrohornblende.  Average of 10 analyses:

SiO2	41.50
Al203	12.97
FeO	15.43
MgO	11.49
CaO	11.34
K2O	1.71
Na2O	1.61
TiO2	2.89
MnO	0.13
	_____
Total	*99.08
	*OH and F not measured.
