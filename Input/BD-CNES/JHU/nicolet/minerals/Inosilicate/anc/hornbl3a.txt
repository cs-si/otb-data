Mineral:  Hornblende Ca2(Fe+2,Mg)4Al(Si7Al)O22(OH,F)2

Sample No.: hornblen.3

XRD Analyis:  Pure magnesiohornblende

Chemistry:  Microprobe analysis shows a slight variation in alumina 
(0.5% to 2.5%) which is, oddly enough, offset by inverse variation in 
magnesia. With that minor exception, the sample appears 
homogeneous and typical magnesiohornblende. Average of 15 
analyses:

SiO2	56.14
Al203	1.24
FeO	6.46
MgO	20.90
CaO	13.17
K2O	0.12
Na2O	0.32
TiO2	0.04
MnO	0.18
	_____
Total	*98.55
	*OH and F not measured.
