Mineral:  Tremolite Ca2(Mg, Fe+2)5Si8O22(OH)2

Sample No.:  tremol.1

XRD Analysis:  Pure tremolite.

Chemistry:  Microprobe analysis showed the sample to be 
homogeneous within and between grains.  Average of 9 analyses:

SiO2	57.97
Al2O3	0.09
FeO	0.08
MgO	24.51
CaO	13.02
K2O	0.13
Na2O	0.17
TiO2	0.02
MnO	0.63
Total	*96.62
	*OH not measured.
