Mineral:  Chrysotile Mg3Si2O5(OH)4

Sample No.: chrysot.1

XRD Analysis:  Pure chrysotile.

Chemistry:  Volatile analysis by C. S. Papp, J. D. Sharkey, and K. J. 
Curry, and XRF analysis of the non-volatiles by J. Taggare, A. Bartel, 
and D. Diems, USGS Branch of Analytical Chemistry, Denver, showed:

SiO2	41.50
Al2O3	0.40
FeTO3	1.31
MgO	40.6
CaO	0.09
K2O	<0.02
Na2O	<0.15
TiO2	<0.02
MnO	0.02
P2O5	<0.05
H2O	14.70
Other LOI	0.90
	_____
Total	99.76
