Mineral:  Phlogopite KMg3Si3Al010(F,OH)2

Sample No.: phlogop.1

XRD Analysis:  Pure phlogopite.

Chemistry:  Chemical analysis by R. A., Robie and B. S. Hemingway, 
USGS, Reston showed:

Si02 	40.30
Al203	14.30
Fe203	0.63
Fe0	1.11
Ti02	1.32
Mn0 	0.03
Mg0	26.40
Ca0	0.07
Ba0	0.16
Na20	0.43
K20	10.10
H20+	2.63
H20- 	0.91
F	3.20
	_____
Total	*100.68
		*Without H2O
