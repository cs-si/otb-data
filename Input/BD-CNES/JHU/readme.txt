The JHU site is arranged in a hierarchial directory structure. At the top level
is the owner (JHU), at the next level is the instrument (becknic and nicolet). The nicolet 
is spectrometer, the becknic indicates the subsequent data are "seamless" spectra coming from 
the beckman and nicolet and covering the combined wavelength range of the two instruments.
The next level is the type (e.g. lunar, manmade, rocks, etc.) and the next level is the 
class (e.g. igneous, metamorphic, sedimentary). Note not all the types have classes. At 
next level is the particle size (again not all classes have several particle size). At the
lowest level are 2 subdirectories (anc and txt). The anc directory includes all the 
ancillary information files and the txt directory includes all the data files. There is also
another text file which provides more detail about that type e.g. soils.txt.