ASTER SPECTRAL LIBRARY Ver. 1.2 README FILE

About the Spectral Library 
Welcome to the ASTER spectral library. The spectral library is a collection of 
spectra of natural and man made materials.

What is in the Spectral Library? 
The ASTER spectral library is currently made up of three other spectral 
libraries: the Jet Propulsion Laboratory (JPL) Spectral Library, the Johns 
Hopkins University (JHU) Spectral Library, and the United States Geological 
Survey (USGS - Reston) Spectral Library.

How are the Data organized on the CD-ROM?  
The data are separated into 4 directories:
	1) JHU - this contains the data contributed by Johns Hopkins.
	2) JPL - this contains the data contributed by the Jet Propulsion 	
		Laboratory.
	3) USGS - this contains the data contributed by the United States 	
		Geological Survey Reston Office.
	4) DOCS - this contains the documents that describe the measurement 	
		technique and instrumentation.

Contribution Directories
Each of the contribution directories (JHU, JPL, USGS) have subdirectories which 
refer to the type of instrument used to measure the spectra contained beneath 
them. For example the JHU directory has 2 subdirectories: becknic and nicolet. 
Data in the becknic directory are "seamless" spectra derived from a Beckman and 
Nicolet spectrometers. Data in the nicolet subdirectory are measured with a 
Nicolet spectrometer. The becknic and nicolet directories are divided into 
subdirectories according to material class, e.g. meteorites. The data may be 
further subdivided by directory, if appropriate, until a final txt directory is 
reached which contains the data files. Each data file is in a standard format. 
In some cases there is an anc directory at the same directory level as the txt 
directory that includes information that is not part of the standard spectral 
file format, e.g. X-Ray information. The standard spectral files always have the 
same number header lines and these are in the format parameter: value. 
