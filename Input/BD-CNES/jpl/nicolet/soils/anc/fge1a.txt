Fine to medium grained quartz rich silty sandstone (finer than 80um) with a 
kaolinite, illite matrix and some calcrete coating.

Clay mineralogy  : XRD analysis indicates the presence of kaolinite and 
muscovite/illite. PIMA SWIR spectra confirms their presence as well as the 
montmillonite and calcite.

Coarse mineralogy : XRF normative analysis estimated 73% quartz, 6% 
illite/muscovite, 4% kaolinite, 6% smectite and 10% calcite.

Spectral Description:  The presence of the 12.4-12.8 um doublet suggests the 
medium grained quartz texture. Spectral features suggestive of kaolinite are 
observed at 9.0, 9.9 and 11.0 um in the FTIR spectrum. 

