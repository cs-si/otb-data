Lithic fluvial alluvium containing phyllite and shale grits, with some quartz-
rich sands. Grain size analysis of fraction finer than 2 mm indicates 35% 
proportion finer than 20 um (12% finer than 5 um).

Clay mineralogy : XRD analysis indicates mainly illite/muscovite and minor 
kaolinite/chlorite.

Coarse mineralogy : XRF-normative analysis estimated this alluvium consisted of 
59% quartz, 16% illite/muscovite and 20% smectite.

Spectral Description: The FTIR spectrum for fgg003 (sieved for fraction less 
than 2 mm) indicates the quartz reststrahlen features at 8.2 and 8.625 um. 
However the subdued longer wavelength reststrahlen (9.3 um), the absence of the 
doublet feature at 12.52 and 12.85 um, and the presence of the 6.4 um feature is 
suggestive of a fine grained quartz texture. The presence of clay is also 
suggested by the broadened longer wavelength quartz reststrahlen feature at 9.3 
um. The TIR shoulder at 6.5-7.0 um and subtle rise 11-11.8 um is possibly due to 
the presence of muscovite.

