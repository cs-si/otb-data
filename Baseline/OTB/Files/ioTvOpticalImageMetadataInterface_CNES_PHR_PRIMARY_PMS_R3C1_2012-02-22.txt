GetSunElevation:     23.7112
GetSunAzimuth:       163.883
GetSatElevation:     8.34658
GetSatAzimuth:       179.822
GetPhysicalBias:     [0, 0, 0, 0]
GetPhysicalGain:     [9.1, 9.63, 10.74, 16.66]
GetSolarIrradiance:  [999, 999, 999, 999]
GetFirstWavelengths: [0.44, 0.5, 0.61, 0.77]
GetLastWavelengths:  [0.54, 0.6, 0.71, 0.91]
GetSpectralSensitivity:  ObjectList (0x2707dc0)
  RTTI typeinfo:   otb::ObjectList<otb::FilterFunctionValues>
  Reference Count: 1
  Modified Time: 147
  Debug: Off
  Observers: 
    none
  Source: (none)
  Source output name: (none)
  Release Data: Off
  Data Released: False
  Global Release Data: Off
  PipelineMTime: 0
  UpdateMTime: 0
  RealTimeStamp: 0 seconds
  Size: 0
  List contains : 

